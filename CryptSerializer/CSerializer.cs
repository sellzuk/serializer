﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace CryptSerializer
{
    public static class CSerializer
    {
        public static string Encryption(string plain)
        {
            string strCipher;
            try
            {
                Console.WriteLine("call CryptoWrapper::Encrypt");
                strCipher = Encrypt(plain);
                Console.WriteLine("=====================================================================================");
                Console.WriteLine(strCipher);
            } 
            catch (Exception ex)
            {
                throw ex;
            }

            return strCipher;
        }

        public static string Decryption(string cipher)
        {
            string strPlain;
            try
            {
                strPlain = Decrypt(cipher);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strPlain;
        }

        [DllImport("CryptoWrapper.dll")]
        [return: MarshalAs(UnmanagedType.LPWStr)]
        public static extern string Encrypt([MarshalAs(UnmanagedType.LPWStr)] string plain);
        [DllImport("CryptoWrapper.dll")]
        [return: MarshalAs(UnmanagedType.LPWStr)]
        public static extern string Decrypt([MarshalAs(UnmanagedType.LPWStr)] string cipher);
    }
}
